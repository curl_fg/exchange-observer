#!/bin/bash
typeset -r POLYBAR_OUT=true LOCAL_CURRENCY='eur' BAD='#F46067' GOOD='#5294E2' \
    RED=$'\e[31m' BOLD=$'\e[1m' BLUE=$'\e[34m' END=$'\e[0m' SCRIPT="${0##*/}" \
    BASE_API="https://api.coingecko.com/api/v3/simple/price?include_24hr_change=true&ids="

typeset TF_CHANGE RES CURR_LOCAL CURR_USD USD_LOCAL

typeset -i CURRENCY=0 DELAY="${1:-60}" LAST_INDX SLEEP_PID
typeset -a ICONS COINS

CLEAN() { rm -f "/tmp/.${USER}_${SCRIPT%.*}.lock"; }

# We ensure that there is only one execution of this script
exec 100>"/tmp/.${USER}_${SCRIPT%.*}.lock" || exit 1
flock -n 100 || exit 1

# List of coins to be observed with their respective icons/chars
COINS=(bitcoin dogecoin zcash ethereum solana iota)
ICONS=("${bitcoin:-₿}" "${dogecoin:-Ð}" "${zcash:-ⓩ}" "${ethereum:-◊}"
    "${solana:-S}" "${iota:-☄}")

LAST_INDX=$((${#COINS[@]} - 1))

REQUEST_API() {
    RES=""

    if RES="$(wget -T5 -qO- "${BASE_API}${COINS[${1}]}&vs_currencies=${LOCAL_CURRENCY},usd" \
        2>/dev/null)"; then

        CURR_LOCAL="$(jq -r ".${COINS[${1}]}.${LOCAL_CURRENCY}" <<<"$RES" 2>/dev/null)"
        CURR_USD="$(jq -r ".${COINS[${1}]}.usd" <<<"$RES" 2>/dev/null)"

        USD_LOCAL="$( (bc 2>/dev/null | awk '{ if (match($0, /\.?[0]+$/)) {
			print substr($0,1,RSTART-1) } else { print $0 }}' 2>/dev/null) \
            <<<"scale=2; $CURR_LOCAL/$CURR_USD")"

        TF_CHANGE="$(jq -r ".${COINS[${1}]}.usd_24h_change" <<<"$RES" \
            2>/dev/null)"
    fi
}

UPDATE_RIGHT() {
    if ((CURRENCY >= LAST_INDX)); then
        CURRENCY=0
    else
        ((CURRENCY++))
    fi

    printf -v CURR_LOCAL '%.0s_' $(seq 1 ${#CURR_LOCAL})
    printf -v CURR_USD '%.0s_' $(seq 1 ${#CURR_USD})

    printf " %s %s, %s  %s %s\n" "${ICONS[$CURRENCY]}" \
        "$CURR_USD" "$CURR_LOCAL" "${dollar:-$}" "$USD_LOCAL"

    kill "$SLEEP_PID" &>/dev/null
}

UPDATE_LEFT() {
    if ((CURRENCY <= 0)); then
        CURRENCY=$LAST_INDX
    else
        ((CURRENCY--))
    fi

    printf -v CURR_LOCAL '%.0s_' $(seq 1 ${#CURR_LOCAL})
    printf -v CURR_USD '%.0s_' $(seq 1 ${#CURR_USD})

    printf " %s %s, %s  %s %s\n" "${ICONS[$CURRENCY]}" \
        "$CURR_USD" "$CURR_LOCAL" "${dollar:-$}" "$USD_LOCAL"

    kill "$SLEEP_PID" &>/dev/null
}

trap UPDATE_RIGHT USR1
trap UPDATE_LEFT USR2
trap CLEAN EXIT

while :; do
    REQUEST_API $CURRENCY

    if [[ "$CURR_USD" =~ _ || "$CURR_LOCAL" =~ _ || "$USD_LOCAL" =~ _ ]]; then
        kill "$SLEEP_PID" &>/dev/null
    else
        if [[ $(bc <<<"$TF_CHANGE > 0" 2>/dev/null) = 1 ]]; then
            $POLYBAR_OUT &&
                FORMAT=" %s %%{F$GOOD}%s%%{F-}, %%{F$GOOD}%s%%{F-}  %s %s\n" ||
                FORMAT=" %s $BLUE$BOLD%s$END, $BLUE$BOLD%s$END  %s %s\n"
        elif [[ $(bc <<<"$TF_CHANGE < 0" 2>/dev/null) = 1 ]]; then
            $POLYBAR_OUT &&
                FORMAT=" %s %%{F$BAD}%s%%{F-}, %%{F$BAD}%s%%{F-}  %s %s\n" ||
                FORMAT=" %s $RED$BOLD%s$END, $RED$BOLD%s$END  %s %s\n"
        else
            FORMAT=" %s %s, %s  %s %s\n"
        fi

        # shellcheck disable=SC2059
        printf "$FORMAT" "${ICONS[$CURRENCY]}" "$CURR_USD" "$CURR_LOCAL" \
            "${dollar:-$}" "$USD_LOCAL"
    fi

    sleep "$DELAY" &
    SLEEP_PID=$!
    wait
done