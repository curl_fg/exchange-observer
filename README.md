# Exchange observer

This script was primarily intended to be used in
[Polybar](https://polybar.github.io/) but this is not necessary.

Using the [CoinGecko](https://www.coingecko.com/en/api/documentation)
public API it shows the current price of your favorite cryptocurrencies, the
format is defined as: Value in USD, value in local currency and the exchange
rate of your local currency against the dollar; this format and the number of
currencies to show are customizable you just need to know a little bit of shell
script.

The script is intended to update the price every 60s, to rotate between your
list of currencies, you only need to send the signal USR1 to rotate right and
USR2 to rotate left.

![](./polybar.png)

## Polybar configuration

If you have configured Polybar modules this is the recommended configuration,
feel free to change it to your liking.

```config
[module/exchange]
type = custom/script
exec = ~/path/to/exchange.sh
tail = true
interval = 60
format = " <label>"
format-padding = 1
click-left = bash -c 'xargs -rI pid kill -12 pid < <(pgrep -of exchange.sh)'
click-right = bash -c 'xargs -rI pid kill -10 pid < <(pgrep -of exchange.sh)'
```

### Dependencies

- `jq` -- To read JSON from the API
- `bc` -- To support divisions with floats