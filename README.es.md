# Exchange observer

Este script se pensó principalmente para ser usado en
[Polybar](https://polybar.github.io/) pero esto no es necesario.

Usando la API publica de
[CoinGecko](https://www.coingecko.com/en/api/documentation) se muestra el
precio actual de sus criptomonedas favoritas, el formato está definido como:
Valor en USD, valor en moneda local y el cambio de su moneda local respecto al
dolar; este formato y el número de monedas a mostrar son personalizables sólo
necesita saber un poco de shell script.

El script está pensado actualizar el precio cada 60s, para rotar entre su lista
de monedas, sólo necesita mandar la señal USR1 para rotar a la derecha y USR2
para rotar a la izquierda.

![](./polybar.png)

## Configuración en Polybar

Si has configurado módulos de Polybar esta es la configuración recomendada,
siéntase libre de cambiarla a su gusto.

```config
[module/exchange]
type = custom/script
exec = ~/path/to/exchange.sh
tail = true
interval = 60
format = " <label>"
format-padding = 1
click-left = bash -c 'xargs -rI pid kill -12 pid < <(pgrep -of exchange.sh)'
click-right = bash -c 'xargs -rI pid kill -10 pid < <(pgrep -of exchange.sh)'
```

### Dependencias

- `jq` -- Para leer el JSON del API
- `bc` -- Para soportar divisiones con flotantes